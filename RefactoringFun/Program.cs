using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefactoringFun
{
    /// <summary>
    /// Objective: 
    /// We are builidng a simple tool to calculate the Body Mass Index (BMI)
    /// Inputs are the user's height (cm) and weight (kg).
    /// These two information will be used to calculate the BMI and display the status.
    /// 
    /// The formula to calculate BMI is:
    /// BMI = Weight(kg)/(Height(m) * Height(m))
    /// Example: 55kg/(1.6m*1.6m) = 21.48
    /// 
    /// Refer to the table below on BMI nutritional status:
    /// |   BMI         | Nutritional status |
    /// ======================================
    /// | Below 18.5    | Underweight        |
    /// | 18.5–24.9     | Normal weight      |
    /// | 25.0–29.9     | Pre-obesity        |
    /// | 30.0–34.9     | Obesity class II   |
    /// | 35.0–39.9     | Underweight        |
    /// | Above 40      | Obesity class III  |
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the tool to calculate you BMI");
            Console.WriteLine("Weight (kg):");
            var w = Console.ReadLine();
            Console.WriteLine("Height (m)");
            var h = Console.ReadLine();

            var intW = int.Parse(w);
            var intH = int.Parse(h);

            var bmi = intW / (intH * intH);
            
            if (bmi < 18.5)
            {
                Console.WriteLine(string.Format("Your BMI is {0}, nutritional status: Underweight", bmi));
            }
            if (bmi > 18.5 && bmi < 24.9)
            {
                Console.WriteLine(string.Format("Your BMI is {0}, nutritional status: Normal weight", bmi));
            }
            if (bmi > 25.0 && bmi < 29.9)
            {
                Console.WriteLine(string.Format("Your BMI is {0}, nutritional status: Pre-obesity", bmi));
            }
            if (bmi > 30.0 && bmi < 34.9)
            {
                Console.WriteLine(string.Format("Your BMI is {0}, nutritional status: Obesity class II", bmi));
            }
            if (bmi > 40)
            {
                Console.WriteLine(string.Format("Your BMI is {0}, nutritional status: Obesity class III", bmi));
            }

            Console.ReadKey();
        }
    }
}
