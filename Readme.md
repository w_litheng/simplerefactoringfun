    /// Objective: 
    /// We are builidng a simple tool to calculate the Body Mass Index (BMI)
    /// Inputs are the user's height (cm) and weight (kg).
    /// These two information will be used to calculate the BMI and display the status.
    /// 
    /// The formula to calculate BMI is:
    /// BMI = Weight(kg)/(Height(m) * Height(m))
    /// Example: 55kg/(1.6m*1.6m) = 21.48
    /// 
    /// Refer to the table below on BMI nutritional status:
    /// |   BMI         | Nutritional status |
    /// ======================================
    /// | Below 18.5    | Underweight        |
    /// | 18.5–24.9     | Normal weight      |
    /// | 25.0–29.9     | Pre-obesity        |
    /// | 30.0–34.9     | Obesity class II   |
    /// | 35.0–39.9     | Underweight        |
    /// | Above 40      | Obesity class III  |